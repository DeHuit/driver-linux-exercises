#include <linux/module.h>	/* Needed by all modules */
#include <linux/kernel.h>	/* Needed for KERN_INFO */
#include <linux/init.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/blkdev.h> // request queue
#include <linux/blk_types.h> // bio
#include <linux/moduleparam.h>
#include <linux/ioctl.h> // ioctl
//#include <linux/stats.h> // Parameter permissions

#define MIN(a,b) (((a)<(b))?(a):(b))
#define print(s) printk(KERN_INFO s)

char partition_name[4] = "EET";
unsigned int major = 0;

// Module parapeters
#define BLOCK_MINORS 2
#define NB_BLOCKS 1
#define SECTOR_SIZE 512
int NB_SECTOR = 1024;
module_param(NB_SECTOR, int, S_IRUGO);

// DEV
static struct rb_device {
  unsigned int size;
  spinlock_t lock;
  struct request_queue *rb_queue;
  struct gendisk *rb_disk;
  char * data;
} rb_dev;
// fops
struct block_device_operations rb_fops;

/**
* Operations
*/
int rb_open(struct block_device * dev, fmode_t mode) { return 0; }
int rb_getgeo(struct block_device * dev, struct hd_geometry * geom) { return 0; }
void rb_close(struct gendisk * gd, fmode_t mode) {}
static int ioctl(struct block_device *, fmode_t, unsigned, unsigned long);


#define READ 0
#define WRITE 1
int rb_transfer(struct request * req) {
  int nbSectorsToTransfer = 0;
  int offset_in_sectors = 0;
  struct bio * bio = req->bio;
  struct bio_vec bio_v;
  struct req_iterator req_iter;
  char * buffer;
  int dir = bio_data_dir(bio); // Step 1
  int first_sector = blk_rq_pos(req); // Step 2
  int nb_sectors = blk_rq_sectors(req); // Step 3


  rq_for_each_segment (bio_v, req, req_iter) { // Step 4
    buffer = page_address(bio_v.bv_page) + bio_v.bv_offset; // Step 4.1
    if (bio_v.bv_len % SECTOR_SIZE != 0) { // Step 4.2
      print("Wrong buffer size");
      return -1;
    }

    nbSectorsToTransfer = bio_v.bv_len / SECTOR_SIZE; // Step 4.3
    if (dir == WRITE) { // Step 4.4 - r/w operation
      memcpy(rb_dev.data + (first_sector+offset_in_sectors)*SECTOR_SIZE, buffer, SECTOR_SIZE*nbSectorsToTransfer);
    } else if (dir == READ) {
      memcpy(buffer, rb_dev.data + (first_sector+offset_in_sectors)*SECTOR_SIZE, nbSectorsToTransfer * SECTOR_SIZE);
    }
    offset_in_sectors += nbSectorsToTransfer;
  }

  if (offset_in_sectors == nb_sectors)
    return 0;
  else
    return -2;
}

// request function
void rb_request(struct request_queue *q) {

  //Extraction
  int ret;
  struct request * req = blk_fetch_request(q);
  //Execution
  print("[request] : Staring processing requests\n");
  while (req) {
    ret = rb_transfer(req);
    __blk_end_request_all(req, ret); // Request processed
    req = blk_fetch_request(q);
  }
  print("[request] : All requests precessed\n");
}

/**
*  INITS AND CLEANUP
*/
int TP1_init_module(void) {
	print("[Module init]\n");
  printk("NB_SECTOR = %d", NB_SECTOR);
  // Setp 1 : module registration
  if ((major = register_blkdev(major, partition_name)) < 0) {
    print("[ERROR] : register_blkdev\n");
    return -EINVAL;
  }
  // Step 2 : request method association
  spin_lock_init (&rb_dev.lock);
  //struct request_queue * queue;
  if ((rb_dev.rb_queue = blk_init_queue(rb_request, &rb_dev.lock)) == NULL) {
    print("[ERROR] : init_queue\n");
    return -ENOMEM;
  }
  blk_queue_logical_block_size(rb_dev.rb_queue, SECTOR_SIZE);
  rb_dev.rb_queue->queuedata = &rb_dev;

  //Init fops
  rb_fops.owner = THIS_MODULE;
  rb_fops.open = rb_open;
  rb_fops.release = rb_close;
  rb_fops.getgeo = rb_getgeo;
  rb_fops.ioctl = ioctl;
  // Step 3 : gendisk allocation
  rb_dev.rb_disk = alloc_disk(BLOCK_MINORS);
  if (rb_dev.rb_disk == NULL) {
    print("RB_DISK NOT ALLOCATED");
    return -ENOMEM;
  }

  rb_dev.size = NB_SECTOR*SECTOR_SIZE*NB_BLOCKS;
  rb_dev.rb_disk->major = major;
  rb_dev.rb_disk->first_minor = 0;
  rb_dev.rb_disk->fops = &rb_fops;
  rb_dev.rb_disk->queue = rb_dev.rb_queue;
  rb_dev.rb_disk->private_data = &rb_dev;
  memcpy(rb_dev.rb_disk->disk_name, partition_name, 4*sizeof(char));

  // Step 4 : gendisk setup
  rb_dev.data = kmalloc(NB_SECTOR*SECTOR_SIZE*NB_BLOCKS, GFP_KERNEL);
  set_capacity(rb_dev.rb_disk, NB_SECTOR);
  add_disk(rb_dev.rb_disk);

	return 0;
}

void TP1_cleanup_module(void) {
  print("[Module cleanup]\n");
  if (rb_dev.rb_disk)
    del_gendisk(rb_dev.rb_disk);
  put_disk(rb_dev.rb_disk);
  blk_cleanup_queue(rb_dev.rb_queue);
  unregister_blkdev(major, partition_name);
  kfree(rb_dev.data);
}

/********************** IOCTL ********************/
#define CRYPT_CMD 1
#define DECRYPT_CMD 0

#define SAMPLE_IOC_MAGIC 'k'
#define CRYPT _IO(SAMPLE_IOC_MAGIC, CRYPT_CMD)
#define DECRYPT _IO(SAMPLE_IOC_MAGIC, DECRYPT_CMD)
#define SAMPLE_IOC_MAXNR 1

#define KEY_LENGTH_MAX 20

void xor_crypt(const char * key, int key_l) {
  int i, key_i = 0;
  print("[ioctl] Encripting...");
  for (i = 0, key_i = 0; i < NB_SECTOR * SECTOR_SIZE; i++, key_i++) {
    if (key_l == key_i)
      key_i = 0;
    rb_dev.data[i] = rb_dev.data[i] ^ key[key_i];
  }
  print("[ioctl] Encripted");
}

static int ioctl(struct block_device * bd, fmode_t fm, unsigned int cmd, unsigned long arg) {
  int key_l = 0;
  char key[KEY_LENGTH_MAX] = {0};

  if (_IOC_TYPE(cmd) != SAMPLE_IOC_MAGIC) return -ENOTTY;
  if (_IOC_NR(cmd) > SAMPLE_IOC_MAXNR) return -ENOTTY;
  if (key == NULL) return -ENOTTY;

  // COpy key from user space
  if (copy_from_user(key, (char *) arg , KEY_LENGTH_MAX) != 0) {
    printk(KERN_ALERT "[ioctl] ERROR COPYING FROM USER SPACE");
    return -EIO;
  }

  while (key_l < KEY_LENGTH_MAX && key[key_l] != '\0')
    key_l++;

  if (key_l == 0) {
    print("[ioctl] zero key length");
    return -EIO;
  }

  printk("[ioctl] Key \'%s\' with length = %d",key, key_l);

  switch(cmd) {
      case CRYPT:
      case DECRYPT: xor_crypt(key, key_l); break;
      default: return -ENOTTY;
  }
  return 0;
}

/* SET UP */

#define LICENCE "GPL V2"
#define AUTHOR "Nikita Kruglov nikita.kruglov@univ-tlse3.fr"
#define DESCRIPTION "TP3"
#define DEVICE "Mi Laptop pro"

module_init(TP1_init_module);
module_exit(TP1_cleanup_module);

MODULE_LICENSE(LICENCE);
MODULE_AUTHOR(AUTHOR);
MODULE_DESCRIPTION(DESCRIPTION);
MODULE_SUPPORTED_DEVICE(DEVICE)
