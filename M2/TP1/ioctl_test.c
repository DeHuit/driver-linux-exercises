#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define CRYPT_CMD 1
#define DECRYPT_CMD 0
#define SAMPLE_IOC_MAGIC 'k'
#define CRYPT _IO(SAMPLE_IOC_MAGIC, CRYPT_CMD)
#define DECRYPT _IO(SAMPLE_IOC_MAGIC, DECRYPT_CMD)

#define KEY_LENGTH_MAX 20
char key[KEY_LENGTH_MAX] = "123";

int main(int argc, char* argv[])
{
  int file;
  if(argc ==2){
    printf("Doing ioctl crypt/decrypt on %s\n",argv[1]);
    file = open(argv[1], O_RDONLY);
    if(file < 0){
      perror("open");
      printf("Error opening file %s!\n",argv[1]);
      return(-1);
    }
    printf("Key = %s\n", key);
    ioctl(file, CRYPT, key);
    close(file);
  }
  else
    printf("usage: %s <filename>\n", argv[0]);
  return 0;
}
