/* *****************************************************************************
 *
 * _____________________________________________________________________________
 *
 *                    I2C DRIVER
 * _____________________________________________________________________________
 *
 * Titre            : implementation of i2c driver
 * Version          : tp2 partie 2
 * Date de creation : 23/09/2019
 * Auteur           : KRUGLOV Nikita
 * Contact          : 
 * Web page         :
 * Collaborateur    : ...
 * Processor        : ...
 * Tools used       : a simple PC (rpi)
 * Compiler         : C
 * Programmateur    : ...
 * Note             :
 *******************************************************************************
 *******************************************************************************
 */

/*------------------------------------> I N C L U D E S <------------------------------*/
//_______________________________________________________________________________________
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/kdev_t.h>
#include <asm/uaccess.h>
#include <linux/uaccess.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/list.h>
#include <linux/blk_types.h> // to have struct bio
#include <linux/blkdev.h>
#include <linux/genhd.h>
#include <linux/ioctl.h>
#include <linux/version.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/ktime.h>


/*=============== ADS1015 ZONE ==============*/
/* THIS PART IS TAKEN "AS IS" FROM ADS1050 GITHUB PAGE */ 
/* I DONT PRETEND TO WRITE THIS */
/*=========================================================================
    I2C ADDRESS/BITS
    -----------------------------------------------------------------------*/
    #define ADS1015_ADDRESS                 (0x48)    // 1001 000 (ADDR = GND)
/*=========================================================================*/

/*=========================================================================
    CONVERSION DELAY (in mS)
    -----------------------------------------------------------------------*/
    #define ADS1015_CONVERSIONDELAY         (1)
    #define ADS1115_CONVERSIONDELAY         (8)
/*=========================================================================*/

/*=========================================================================
    POINTER REGISTER
    -----------------------------------------------------------------------*/
    #define ADS1015_REG_POINTER_MASK        (0x03)
    #define ADS1015_REG_POINTER_CONVERT     (0x00)
    #define ADS1015_REG_POINTER_CONFIG      (0x01)
    #define ADS1015_REG_POINTER_LOWTHRESH   (0x02)
    #define ADS1015_REG_POINTER_HITHRESH    (0x03)
/*=========================================================================*/

/*=========================================================================
    CONFIG REGISTER
    -----------------------------------------------------------------------*/
    #define ADS1015_REG_CONFIG_OS_MASK      (0x8000)
    #define ADS1015_REG_CONFIG_OS_SINGLE    (0x8000)  // Write: Set to start a single-conversion
    #define ADS1015_REG_CONFIG_OS_BUSY      (0x0000)  // Read: Bit = 0 when conversion is in progress
    #define ADS1015_REG_CONFIG_OS_NOTBUSY   (0x8000)  // Read: Bit = 1 when device is not performing a conversion

    #define ADS1015_REG_CONFIG_MUX_MASK     (0x7000)
    #define ADS1015_REG_CONFIG_MUX_DIFF_0_1 (0x0000)  // Differential P = AIN0, N = AIN1 (default)
    #define ADS1015_REG_CONFIG_MUX_DIFF_0_3 (0x1000)  // Differential P = AIN0, N = AIN3
    #define ADS1015_REG_CONFIG_MUX_DIFF_1_3 (0x2000)  // Differential P = AIN1, N = AIN3
    #define ADS1015_REG_CONFIG_MUX_DIFF_2_3 (0x3000)  // Differential P = AIN2, N = AIN3
    #define ADS1015_REG_CONFIG_MUX_SINGLE_0 (0x4000)  // Single-ended AIN0
    #define ADS1015_REG_CONFIG_MUX_SINGLE_1 (0x5000)  // Single-ended AIN1
    #define ADS1015_REG_CONFIG_MUX_SINGLE_2 (0x6000)  // Single-ended AIN2
    #define ADS1015_REG_CONFIG_MUX_SINGLE_3 (0x7000)  // Single-ended AIN3

    #define ADS1015_REG_CONFIG_PGA_MASK     (0x0E00)
    #define ADS1015_REG_CONFIG_PGA_6_144V   (0x0000)  // +/-6.144V range = Gain 2/3
    #define ADS1015_REG_CONFIG_PGA_4_096V   (0x0200)  // +/-4.096V range = Gain 1
    #define ADS1015_REG_CONFIG_PGA_2_048V   (0x0400)  // +/-2.048V range = Gain 2 (default)
    #define ADS1015_REG_CONFIG_PGA_1_024V   (0x0600)  // +/-1.024V range = Gain 4
    #define ADS1015_REG_CONFIG_PGA_0_512V   (0x0800)  // +/-0.512V range = Gain 8
    #define ADS1015_REG_CONFIG_PGA_0_256V   (0x0A00)  // +/-0.256V range = Gain 16

    #define ADS1015_REG_CONFIG_MODE_MASK    (0x0100)
    #define ADS1015_REG_CONFIG_MODE_CONTIN  (0x0000)  // Continuous conversion mode
    #define ADS1015_REG_CONFIG_MODE_SINGLE  (0x0100)  // Power-down single-shot mode (default)

    #define ADS1015_REG_CONFIG_DR_MASK      (0x00E0)  
    #define ADS1015_REG_CONFIG_DR_128SPS    (0x0000)  // 128 samples per second
    #define ADS1015_REG_CONFIG_DR_250SPS    (0x0020)  // 250 samples per second
    #define ADS1015_REG_CONFIG_DR_490SPS    (0x0040)  // 490 samples per second
    #define ADS1015_REG_CONFIG_DR_920SPS    (0x0060)  // 920 samples per second
    #define ADS1015_REG_CONFIG_DR_1600SPS   (0x0080)  // 1600 samples per second (default)
    #define ADS1015_REG_CONFIG_DR_2400SPS   (0x00A0)  // 2400 samples per second
    #define ADS1015_REG_CONFIG_DR_3300SPS   (0x00C0)  // 3300 samples per second

    #define ADS1015_REG_CONFIG_CMODE_MASK   (0x0010)
    #define ADS1015_REG_CONFIG_CMODE_TRAD   (0x0000)  // Traditional comparator with hysteresis (default)
    #define ADS1015_REG_CONFIG_CMODE_WINDOW (0x0010)  // Window comparator

    #define ADS1015_REG_CONFIG_CPOL_MASK    (0x0008)
    #define ADS1015_REG_CONFIG_CPOL_ACTVLOW (0x0000)  // ALERT/RDY pin is low when active (default)
    #define ADS1015_REG_CONFIG_CPOL_ACTVHI  (0x0008)  // ALERT/RDY pin is high when active

    #define ADS1015_REG_CONFIG_CLAT_MASK    (0x0004)  // Determines if ALERT/RDY pin latches once asserted
    #define ADS1015_REG_CONFIG_CLAT_NONLAT  (0x0000)  // Non-latching comparator (default)
    #define ADS1015_REG_CONFIG_CLAT_LATCH   (0x0004)  // Latching comparator

    #define ADS1015_REG_CONFIG_CQUE_MASK    (0x0003)
    #define ADS1015_REG_CONFIG_CQUE_1CONV   (0x0000)  // Assert ALERT/RDY after one conversions
    #define ADS1015_REG_CONFIG_CQUE_2CONV   (0x0001)  // Assert ALERT/RDY after two conversions
    #define ADS1015_REG_CONFIG_CQUE_4CONV   (0x0002)  // Assert ALERT/RDY after four conversions
    #define ADS1015_REG_CONFIG_CQUE_NONE    (0x0003)  // Disable the comparator and put ALERT/RDY in high state (default)
/*=========================================================================*/

/*=============== END ADS1015 ZONE ==============*/

/*------------------------------------> M O D U L E - D E S C R I P T I O N <----------*/
//_______________________________________________________________________________________
MODULE_AUTHOR("KRUGLOV Nikita");
MODULE_DESCRIPTION("TP3");
MODULE_SUPPORTED_DEVICE("none");
MODULE_LICENSE("GPL v2");
/*------------------------------------> D E F I N I T I O N <--------------------------*/
//_______________________________________________________________________________________
#define DEBUG
#define HIGH 1
#define LOW 0
#define CLOCK_SPEED_MS 5 // (1/clock speed) / 2

/*------------------------------------> G L O B A L - P A R A M E T R E <---------------*/
//_______________________________________________________________________________________

//##############################
// GPIO
//##############################
int SCL = 17;
int SDA = 27;
module_param(SCL, int, S_IRUGO);
module_param(SDA, int, S_IRUGO);

//##############################
// CDEV
//##############################
int channel = 0;
struct cdev *myCDev;
static dev_t dev;

//##############################
// MUTEX
//##############################
static struct mutex myMutex;
static DEFINE_MUTEX(myMutex);

//##############################
// NB PERIPHE
//##############################
static int nbPeriph = 4;

//##############################
// NB PERIPHE
//##############################
int i2cStarted = 0;

/*------------------------------------> P R O T O T Y P E  <---------------------------*/
//_______________________________________________________________________________________

static ssize_t I2C_read(struct file *f, char *buf, size_t size, loff_t *offset);
static ssize_t I2C_write(struct file *filp, const char *buff, size_t count, loff_t *offp);
static int I2C_close(struct inode * inode, struct file *f);
static int I2C_open(struct inode *inode, struct file *f);

/*------------------------------------> L O C A L - P R O T O T Y P E  <---------------*/
//_______________________________________________________________________________________

void I2C_clock(void);

int I2C_SDA_get(void);

int I2C_SCL_get(void);

void I2C_SCL_set(void);

void I2C_SCL_clear(void);

void I2C_SDA_set(void);

void I2C_SDA_clear(void);

void I2C_Start(void);

void I2C_Stop(void);

void I2C_pass_arbotration(void);
/*------------------------------------> S T R U C T U R E - F O P S <------------------*/
//_______________________________________________________________________________________

static struct file_operations myFops = {
    .owner      = THIS_MODULE,
    .llseek     = NULL,
    .read       = I2C_read,
    .write      = I2C_write,
    .unlocked_ioctl = NULL,
    .open = I2C_open,
    .release = I2C_close
};



/*------------------------------------> I M P L E M E N T A T I O N - L O C <-----------*/
//_______________________________________________________________________________________

void I2C_clock(void) {
  udelay(CLOCK_SPEED_MS);
}

// Getters
int I2C_SDA_get(void) {
  return gpio_get_value(SDA);
}

int I2C_SCL_get(void) {
  return gpio_get_value(SCL);
}

// Setters for SCL
void I2C_SCL_set(void) { // Drive SCL to high impedance (HIGH + listener)
  gpio_free(SCL);
  gpio_request_one(SCL, GPIOF_IN, "SCL");
}

void I2C_SCL_clear(void) { // Drive SCL low activelly
  // force the line to down : simulate the pull down resister
  gpio_free(SCL);
  gpio_request_one(SCL, GPIOF_OUT_INIT_LOW, "SCL");
}

// Setters for SDA
void I2C_SDA_set(void) { // Drive SDA to high impedance (HIGH + listener)
  gpio_free(SDA);
  gpio_request_one(SDA, GPIOF_IN, "SDA");
}

void I2C_SDA_clear(void) { // Drive SDA low activelly
  gpio_free(SDA);
  gpio_request_one(SDA, GPIOF_OUT_INIT_LOW, "SDA");
}

void I2C_pass_arbotration(void) {
  // TODO
}

#define TIMEOUT 10

void I2C_Start(void) {
  char time_ms = 0;

  // SDA ‾‾‾\______
  // SCL ‾‾‾‾‾‾‾\__

  if (!i2cStarted) {
    I2C_SDA_set();
    I2C_clock();
    I2C_SCL_set();
    //printk("I2C Start waiting for SCL to go to 0");
    while(I2C_SCL_get() == 0) {
      if (time_ms == 10) {
        printk("[Start] Timer expired");
        return;
      }
      udelay(1);
      time_ms++;
    }
    I2C_clock();
  }

  if (I2C_SDA_get() == 0)
    I2C_pass_arbotration();

  I2C_SDA_clear(); // SDA = 0
  I2C_clock();
  I2C_SCL_clear(); // SCL = 0
  I2C_clock();
  i2cStarted = true;
}

void I2C_Stop(void) {
  char time_ms = 0;

  // SDA ____/‾‾‾‾‾‾‾
  // SCL _/‾‾‾‾‾‾‾‾‾‾
  I2C_SDA_clear();
  I2C_clock();

  I2C_SCL_set();
  while(I2C_SCL_get() == 0)  {
    if (time_ms == 10) {
      printk("[STOP] Timer expired");
      return;
    }
    udelay(1);
    time_ms++;
  }
  I2C_clock();

  I2C_SDA_set();
  I2C_clock();

  if (I2C_SDA_get() == 0)
    I2C_pass_arbotration();

  i2cStarted = false;
}

void I2C_writeBit(bool bit) {
  char time_ms = 0;

  // SDA <bit set>
  // SCL ...‾‾\___ 

  // Set bit
  if (bit) {
    I2C_SDA_set();
  } else {
    I2C_SDA_clear();
  }

  I2C_clock();
  I2C_SCL_set();
  I2C_clock();

  while (I2C_SCL_get() == 0)  {
    if (time_ms == 10) {
      printk("[WRITE] Timer expired");
      return;
    }
    udelay(1);
    time_ms++;
  }

  // When SCL is HIGH, data is valid
  if (bit && (I2C_SDA_get() == 0)) {
    I2C_pass_arbotration();
  }

  I2C_SCL_clear();
}

bool I2C_readBit(void) {
  bool bit;
  char time_ms = 0;

  // SDA <bit set>
  // SCL ...__/‾‾

  I2C_SDA_set();
  I2C_clock();
  I2C_SCL_set();
  I2C_clock(); // Wait for slave to change SDA line
  bit = I2C_SDA_get();
  I2C_clock();
  I2C_SCL_clear();

  return bit;
}

uint8_t I2C_writeByte(uint8_t start, uint8_t stop, uint8_t byte) {
  unsigned bit;
  uint8_t nack;

  if (start)
    I2C_Start();

  for (bit = 0; bit < 8; ++bit) {
    I2C_writeBit((byte & 0x80) != 0);
    byte <<= 1;
  }

  nack = I2C_readBit();

  if (stop)
    I2C_Stop();

  return nack;
}

uint8_t I2C_readByte(bool nack, bool send_stop) {
  uint8_t byte = 0, bit;

  for (bit = 0; bit < 8; ++bit)
    byte = (byte << 1) | I2C_readBit();

  I2C_writeBit(nack);

  if (send_stop)
    I2C_Stop();

  return byte;
}

void I2C_writeRegister(uint8_t i2cAddress, uint8_t reg, uint16_t value) {
  I2C_writeByte(true, false, ADS1015_ADDRESS << 1 | 0); // write to address
  I2C_writeByte(false, false, (uint8_t)reg);
  I2C_writeByte(false, false, (uint8_t)(value>>8));
  I2C_writeByte(false, true, (uint8_t)(value & 0xFF));
}

uint16_t I2C_readRegister(uint8_t i2cAddress, uint8_t reg) {
  uint16_t ret = 0;
  I2C_writeByte(true, false, ADS1015_ADDRESS << 1 | 0); // write to address
  I2C_writeByte(false, true, ADS1015_REG_POINTER_CONVERT);
  I2C_writeByte(true, false, ADS1015_ADDRESS << 1 | 1); // read from address
  ret = I2C_readByte(1, false);
  ret = ret << 8;
  ret += I2C_readByte(1, true);

  return ((ret << 8) | ret);  
}


/*------------------------------------> I M P L E M E N T A T I O N - G E N <-----------*/
//_______________________________________________________________________________________

int I2C_Init(void) {
    //allocation des paires de (majeur, mineur) de façon dynamique
    if (alloc_chrdev_region(&dev, 0, nbPeriph, "adc") < 0) {
        printk(KERN_DEBUG ">> ERROR : alloc_chrdev_region \n");
        return -EINVAL;
    }
    //affichage du majeur et du minneur
    printk(KERN_ALERT "Affichage (majeur = %d, mineur = %d)\n",MAJOR(dev),MINOR(dev));

    //chargement du periph
    //first methode
    myCDev = cdev_alloc();
    myCDev->ops = &myFops;
    myCDev->owner = THIS_MODULE;
    //second methode
    // cedv_init(myCDev, myFops);

    //on lie le periphe et les operations
    cdev_add(myCDev, dev, nbPeriph);

    return 0;
}

static ssize_t I2C_read(struct file *f, char *buf, size_t size, loff_t *offset) {
  uint16_t ret ;
  // Preparing package
  uint16_t config = ADS1015_REG_CONFIG_CQUE_NONE    | // Disable the comparator (default val)
                    ADS1015_REG_CONFIG_CLAT_NONLAT  | // Non-latching (default val)
                    ADS1015_REG_CONFIG_CPOL_ACTVLOW | // Alert/Rdy active low   (default val)
                    ADS1015_REG_CONFIG_CMODE_TRAD   | // Traditional comparator (default val)
                    ADS1015_REG_CONFIG_DR_1600SPS   | // 1600 samples per second (default)
                    ADS1015_REG_CONFIG_MODE_SINGLE;   // Single-shot mode (default)
  config |= 0;
  switch (channel) {
    case (0):
      config |= ADS1015_REG_CONFIG_MUX_SINGLE_0;
      break;
    case (1):
      config |= ADS1015_REG_CONFIG_MUX_SINGLE_1;
      break;
    case (2):
      config |= ADS1015_REG_CONFIG_MUX_SINGLE_2;
      break;
    case (3):
      config |= ADS1015_REG_CONFIG_MUX_SINGLE_3;
      break;
  }
  config |= ADS1015_REG_CONFIG_OS_SINGLE;

  // Write config register to the ADC
  I2C_writeRegister(0, ADS1015_REG_POINTER_CONFIG, config);
  udelay(ADS1015_CONVERSIONDELAY*1000);
  ret = I2C_readRegister(0, ADS1015_REG_POINTER_CONVERT) >> 4;  

  // Write to user
  if (copy_to_user(buf, &ret, 2) != 0)
    return -EFAULT;
  else
    return 2;

}
static ssize_t I2C_write(struct file *filp, const char *buff, size_t count, loff_t *offp) {
  return 0;
}
static int I2C_close(struct inode * inode, struct file *f) {
  return 0;
}
static int I2C_open(struct inode *inode, struct file *f) {
  channel = iminor(inode);
  return 0;
}

static void I2C_Cleanup(void) {
    // liberation
    gpio_free(SDA);
    gpio_free(SCL);
    unregister_chrdev_region(dev, nbPeriph);
    cdev_del(myCDev);
}


module_init(I2C_Init);
module_exit(I2C_Cleanup);
/*------------------------------------> E N D <----------------------------------------*/
//_______________________________________________________________________________________
