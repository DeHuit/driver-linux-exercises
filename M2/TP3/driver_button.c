/* *****************************************************************************
 *
 * _____________________________________________________________________________
 *
 *                    DRIVER ULTRASONIC
 * _____________________________________________________________________________
 *
 * Titre            : implementation of i2c driver
 * Version          : tp2.1
 * Date de creation : 16/09/2019
 * Auteur           : MMADI Anzilane, KRUGLOV Nikita
 * Contact          : anzilan@hotmail.fr
 * Web page         :
 * Collaborateur    : ...
 * Processor        : ...
 * Tools used       : a simple PC (rpi)
 * Compiler         : C
 * Programmateur    : ...
 * Note             :
 *******************************************************************************
 *******************************************************************************
 */
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/kdev_t.h>
#include <asm/uaccess.h>
#include <linux/uaccess.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/list.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/ktime.h>

MODULE_AUTHOR("MMADI Anzilane, KRUGLOV Nikita");
MODULE_DESCRIPTION("driver de capteur ultrasonic");
MODULE_SUPPORTED_DEVICE("none");


#define HIGH 1
#define LOW 0

//prototype de mes fonctions de driver
static ssize_t myRead(struct file *f, char *buf, size_t size, loff_t *offset);
static ssize_t myWrite(struct file *filp, const char *buff, size_t count, loff_t *offp);
static int myRelease(struct inode * inode, struct file *f);
static int myOpen(struct inode *inode, struct file *f);

//declaration de GPIO
#define BUTTON_PIN 28

// structure du temps
struct timespec64 ts;

//declaration d'un periphe de type char
struct cdev *myCDev;
static dev_t dev;

//declaration de mon driver
static struct file_operations myFops = {
    .owner      = THIS_MODULE,
    .llseek     = NULL,
    .read       = myRead,
    .write      = myWrite,
    .unlocked_ioctl = NULL,
    .open = myOpen,
    .release = myRelease
};

//fonction d'initialisation de mon module
int driver_button_init(void) {
    //allocation des paires de (majeur, mineur) de façon dynamique
    if (alloc_chrdev_region(&dev, 0, 1, "button") < 0) {
        printk(KERN_DEBUG "[ERROR] : alloc_chrdev_region \n");
        return -EINVAL;
    }
    //affichage du majeur et du minneur
    printk(KERN_ALERT "[INIT] Major = %d, minor = %d)\n",MAJOR(dev),MINOR(dev));

    //chargement du periph
    //first methode
    myCDev = cdev_alloc();
    myCDev->ops = &myFops;
    myCDev->owner = THIS_MODULE;
    //second methode
    // cedv_init(myCDev, myFops);

    //on lie le periphe et les operations
    cdev_add(myCDev, dev, 1);

    // creation et initialisation des GPIO
    gpio_request_one(BUTTON_PIN, GPIOF_IN | GPIOF_OUT_INIT_LOW, "button"); // TO REVIEW
    return 0;
}

//libération des rsc
static void driver_button_cleanup(void) {
    // liberation
    gpio_free(BUTTON_PIN);
    unregister_chrdev_region(dev,1);
    cdev_del(myCDev);
}

int button_pushed_max = 1;
int button_pushed = 0;
// implementation des fonctioinalités
static ssize_t myRead (struct file *f, char *buf, size_t size, loff_t *pPos) {
    int val = 1, old_val = 1; // Button default value
    button_pushed = 0;
    printk("[READ] : Waiting for button switch\n");
    while (1) {
        old_val = val; // Pus old value
        val = gpio_get_value(BUTTON_PIN); // Get new value          ____
        if (val == 0 && old_val == 1) { // Falling edge detection       \_____
            button_pushed++;
            printk("[READ] : Burron pushed for %d times\n", button_push);
            if (button_push == button_pushed_max)
                break;
        }
    }
    printk("[READ] : Button switch detected for %d\n", button_pushed_max);
    return sizeof(long);
}

#define BUFFER_MAX 5

static ssize_t myWrite(struct file *filp, const char *buf, size_t size, loff_t *pPos) {
    char string[BUFFER_MAX];
    int n = 0, i, pos = 1;
    copy_from_user(string,buf, size);
    if (size == 1) {
        printk("[WRITE] : Value as a char is detected\n");
        n = string[0];
    } else {
        printk("[WRITE] : Value as a string is detected\n");
        size = (size <= BUFFER_MAX)?size:BUFFER_MAX;
        for (i = size; i >= 0; i++) {
            if (0 <= string[i] - '0' &&  string[i] - '0' <= 9) {
                n += (string[i] - '0') * pos;
                pos *= 10;
            }
        }
    }
    button_pushed_max = (0 < n && n < 255)?n:1;
    printk("[WRITE] : New max switches value is %d\n", button_pushed_max);
    return size;
}

static int myRelease(struct inode * inode, struct file *f) {
    printk(KERN_ALERT "demande de fermeture ==> close\n");
    return 0;
}

static int myOpen(struct inode *inode, struct file *f) {
    printk(KERN_ALERT "demande d'ouverture ==> open\n");
    return 0;
}

// appelle des fonction d'initialisation et sortie
module_init(driver_button_init);
module_exit(driver_button_cleanup);

MODULE_LICENSE("GPL v2");
