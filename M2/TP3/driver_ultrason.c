/* *****************************************************************************
 *
 * _____________________________________________________________________________
 *
 *                    DRIVER ULTRASONIC
 * _____________________________________________________________________________
 *
 * Titre            : implementation of i2c driver
 * Version          : tp2.1
 * Date de creation : 16/09/2019
 * Auteur           : MMADI Anzilane, KRUGLOV Nikita
 * Contact          : anzilan@hotmail.fr
 * Web page         :
 * Collaborateur    : ...
 * Processor        : ...
 * Tools used       : a simple PC (rpi)
 * Compiler         : C
 * Programmateur    : ...
 * Note             :
 *******************************************************************************
 *******************************************************************************
 */
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/kdev_t.h>
#include <asm/uaccess.h>
#include <linux/uaccess.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/list.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/ktime.h>

MODULE_AUTHOR("MMADI Anzilane, KRUGLOV Nikita");
MODULE_DESCRIPTION("driver de capteur ultrasonic");
MODULE_SUPPORTED_DEVICE("none");


#define HIGH 1
#define LOW 0

//prototype de mes fonctions de driver
static ssize_t myRead(struct file *f, char *buf, size_t size, loff_t *offset);
static ssize_t myWrite(struct file *filp, const char *buff, size_t count, loff_t *offp);
static int myRelease(struct inode * inode, struct file *f);
static int myOpen(struct inode *inode, struct file *f);

//declaration de GPIO
int trigOutput = 17;
int echoInput = 27;

// structure du temps
struct timespec64 ts;

//declaration d'un periphe de type char
struct cdev *myCDev;
static dev_t dev;

//declaration de mon driver
static struct file_operations myFops = {
    .owner      = THIS_MODULE,
    .llseek     = NULL,
    .read       = myRead,
    .write      = myWrite,
    .unlocked_ioctl = NULL,
    .open = myOpen,
    .release = myRelease
};

//fonction d'initialisation de mon module
int driver_ultrasonic_init(void) {
    //allocation des paires de (majeur, mineur) de façon dynamique
    if (alloc_chrdev_region(&dev, 0, 1, "ultrasonic") < 0) {
        printk(KERN_DEBUG ">> ERROR : alloc_chrdev_region \n");
        return -EINVAL;
    }
    //affichage du majeur et du minneur
    printk(KERN_ALERT "Affichage (majeur = %d, mineur = %d)\n",MAJOR(dev),MINOR(dev));

    //chargement du periph
    //first methode
    myCDev = cdev_alloc();
    myCDev->ops = &myFops;
    myCDev->owner = THIS_MODULE;
    //second methode
    // cedv_init(myCDev, myFops);

    //on lie le periphe et les operations
    cdev_add(myCDev, dev, 1);

    // creation et initialisation des GPIO
    gpio_request_one(trigOutput, GPIOF_OUT_INIT_LOW, "trigger");
    gpio_request_one(echoInput, GPIOF_IN, "echo");
    return 0;
}

//libération des rsc
static void driver_ultrasonic_cleanup(void) {
    // liberation
    gpio_free(trigOutput);
    gpio_free(echoInput);
    unregister_chrdev_region(dev,1);
    cdev_del(myCDev);
}


// implementation des fonctioinalités
static ssize_t myRead (struct file *f, char *buf, size_t size, loff_t *pPos) {
    printk(KERN_ALERT "demande de lecture ultrasonic\n");
    char to_user[5];
    memset(to_user, 0, 5);
    gpio_set_value(trigOutput, HIGH);
    udelay(10);
    gpio_set_value(trigOutput, LOW);

    long end = 0;
    ktime_get_ts64(&ts);
    long start = ts.tv_nsec/1000;
    while (gpio_get_value(echoInput) == 0) {
      ktime_get_ts64(&ts);
      start = ts.tv_nsec/1000;
    }

    while (gpio_get_value(echoInput) == 1) {
      ktime_get_ts64(&ts);
      end = ts.tv_nsec/1000;
    }

    long duree = end - start;
    long distance = (duree*34300) / 2;
    size_t ret = 0;
    if (distance/1000000 < 300)
    	sprintf(to_user, "%d", (int)(distance/1000000));
    if ((ret = copy_to_user(buf, to_user, 5)) < 0) {
      printk(KERN_ALERT "ERROR READ\n");
    }
    printk(KERN_ALERT "dist %s\n", to_user);
    // discussion incomplete ==> si bug voir nikita
    return sizeof(long);
}

static ssize_t myWrite(struct file *filp, const char *buf, size_t size, loff_t *pPos) {

    return 0;
}

static int myRelease(struct inode * inode, struct file *f) {
    printk(KERN_ALERT "demande de fermeture ==> close\n");
    return 0;
}

static int myOpen(struct inode *inode, struct file *f) {
    printk(KERN_ALERT "demande d'ouverture ==> open\n");
    return 0;
}

// appelle des fonction d'initialisation et sortie
module_init(driver_ultrasonic_init);
module_exit(driver_ultrasonic_cleanup);

MODULE_LICENSE("GPL v2");
