#!/usr/bin/python
# coding=utf-8
 
# Les modules nécessaires sont importés et mis en place
import time
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
 
# Sélection des broches d'entrée/sortie respectifs
Output_Trigger = 17
Input_Echo    = 27
 
# Réglage de la pause entre les mesures
sleeptime = 0.8
 
# Configuration des broches d'entrée/sortie
GPIO.setup(Output_Trigger, GPIO.OUT)
GPIO.setup(Input_Echo, GPIO.IN)
GPIO.output(Output_Trigger, False)
 
# Boucle de programme principale
try:
    while True:
        # Le début de la mesure est lancé par un signal de 10µs sur Trigger
        GPIO.output(Output_Trigger, True)
        time.sleep(0.00001)
        GPIO.output(Output_Trigger, False)
 
        # Démarrage du chronomètre
        HeureActive = time.time()
        while GPIO.input(Input_Echo) == 0:
            HeureActive = time.time() # L'heure actuelle est enregistrée jusqu'à ce que le signal soit activé
 
        while GPIO.input(Input_Echo) == 1:
            HeureInactive = time.time() # Enregistrement de l'heure au moment où le signal est encore actif
 
        # La différence en les deux heures donne la durée recherchée
        Duree = HeureInactive - HeureActive
        # Calcul de la distance en fonction de la durée enregistrée
        Distance = (Duree * 34300) / 2
 
        # On vérifie si le signal se trouve dans la plage de mesure du capteur
        if Distance < 2 or (round(Distance) > 300):
            # Si ce n'est pas le cas, affichage d'un signal d'erreur
            print("Distance en-dehors de la portée")
            print("------------------------------")
        else:
            # La distance est formatée à 2 décimales
            Distance = format((Duree * 34300) / 2, '.2f')
            # La distance calculée est envoyée vers la console
            print("La distance mesure:"), Distance,("cm")
            print("------------------------------")
 
        # Pause entre les mesures
        time.sleep(sleeptime)
 
# réinitialisation de tous les GPIO en entrées
except KeyboardInterrupt:
    GPIO.cleanup()