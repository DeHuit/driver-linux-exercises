#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
# include <string.h>
#include <unistd.h>
#include <sys/wait.h>

#define DATA "abcd"
#define DATA_1 "1234"
#define DATA_2 "???"
#define DATA_3 "yes"
#define DATA_SIZE 4
#define DATA_1_SIZE 4
#define DATA_2_SIZE 3
#define DATA_3_SIZE 3

#define READ_SIZE 2
#define READ_SIZE_1

// Safe = 1, unsafe = 0
void wr(int pid, int mode, char * info, int size) {

  int filePointer = open("/dev/w" , O_RDWR) ;
  write(filePointer, info, size);
  close(filePointer);

  char out_info[size];

  int sizeToRead = size; //Set the amount of info needed

  filePointer = open((mode == 1)?"/dev/r_s":"/dev/r" , O_RDWR);
  int sizeRead = read(filePointer, out_info, size);
  out_info[sizeRead] = '\0';
  if (sizeRead > 0) {
    printf("[ %i ] Data safe read result : [%d byte] : %s\n", pid, sizeRead, out_info);
  } else {
    printf("[ %i ] No data found\n", pid);
  }
  close(filePointer) ;

  filePointer = open((mode == 0)?"/dev/r_s":"/dev/r" , O_RDWR);
  sizeRead = read(filePointer, out_info, size);
  out_info[sizeRead] = '\0';
  if (sizeRead > 0) {
    printf("[ %i ] Data read result : [%d byte] : %s\n", pid, sizeRead, out_info);
  } else {
    printf("[ %i ] No data found\n", pid);
  }
  close(filePointer) ;
}

int main(int argc, char * argv[]) {
	int term;
	pid_t son;
	pid_t dad;

	//for (int i = 1; i < argc; i++) {
	  son = fork();
		dad = getpid();
		switch (son) {
			case -1: perror("Erreur");
				 exit(EXIT_FAILURE);
			case  0:
         wr(getpid(), 1, DATA, DATA_SIZE);
				 exit(EXIT_SUCCESS);
		}
	//}
	while ((son = wait(&term)) != -1)
		printf("[ %d ]: fils [%d] est termine. Code de retout: %d\n", (int) dad, (int) son, WEXITSTATUS(term));
	wait(NULL);
  wr(getpid(), 1, DATA_1, DATA_1_SIZE);
  return 0;
}
