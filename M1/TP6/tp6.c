#include <linux/init.h>
#include <linux/module.h>
#include <linux/cdev.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/list.h>
#include <linux/slab.h>

#define LICENCE "GPL V2"
#define AUTHOR "Nikita Kruglov nikita.kruglov@univ-tlse3.fr"
#define DESCRIPTION "TP6"
#define DEVICE "Mi Laptop pro"

#define READ_DEVICE 0
#define READ_DEVICE_SAFE 1
#define WRITE_DEVICE 2

#define MIN(a,b) (((a)<(b))?(a):(b))

// Declaration de fonctions (prefixe par bu)
static ssize_t TP6_read(struct file *, char *, size_t, loff_t *);
static ssize_t TP6_write(struct file *, const char *, size_t, loff_t *);
static int TP6_open(struct inode *, struct file *);
static int TP6_release(struct inode *, struct file *);

//Structure de file_operations
static struct file_operations fops = {
    .read = TP6_read,
    .write = TP6_write,
    .open = TP6_open,
    .release = TP6_release,
};

dev_t dev;
struct cdev * user_cdev;
char need_to_init_buffer;

struct list_head buffer;
struct list_head *pos, *q, *safe_pos;

/*Structure and operators*/
typedef struct s_case{
    char * data;   //data in case
    size_t size;   //current data size
    size_t r;      //current read position
    size_t r_s;      //current safe read position
    struct list_head list;
} Case;

/* Operators */

Case * create_case(const char * data, size_t size) {
  //Step 1 : allocate and set up Case structure
  Case * n;
  n = (Case *) kmalloc(sizeof(Case), GFP_KERNEL);
  n->r = 0;
  n->r_s = 0;
  n->size = size;
  n->data = (char *) kmalloc(sizeof(char) * size, GFP_KERNEL);
  if (copy_from_user(n->data, data, sizeof(char)*size) == 0) {
    printk(KERN_ALERT "New node created");
    return n;
  } else {
    kfree(n->data);
    kfree(n);
    printk(KERN_ALERT "New node creation failed");
    return NULL;
  }
}

ssize_t delete_case(Case *c) {
  kfree(c->data);
  kfree(c);
  return 0;
}

void print_buffer(const char * s) {
    ssize_t i = 0;
    Case * unElement;
    char * buffer_to_str;
    struct list_head *ptr;
    printk(KERN_ALERT "[PRINTING BUFFER]\n");
    list_for_each(ptr, &buffer) {
      unElement = list_entry(ptr, Case, list);
      buffer_to_str = kmalloc(unElement->size+1, GFP_KERNEL);
      for(i = 0; i < unElement->size; i++)
        buffer_to_str[i] = unElement->data[i];
      buffer_to_str[unElement->size] = '\0';
      printk(KERN_ALERT "%s\n", buffer_to_str);
      kfree(buffer_to_str);
    };
    printk(KERN_ALERT "[END PRINTING BUFFER]\n");
}

/* IMPLEMENTATION OF FILE OPERATIONS */
//Flag = 0 -> list wasn't read is the safe mannier yet
//Flag = 1 -> list was read and we need to re-init our safe read device at the end
char list_was_read = 0;

static ssize_t TP6_read_safe(struct file *f, char * buf, size_t size, loff_t *offset) {
    Case * c, *tmp;
    size_t sizeToRead;
    struct list_head * q, *pos;

    sizeToRead = 0;
    //List empty -> 0 byte read
      if (list_empty(&buffer)) {
        return 0;
      }

      // Cae when list is not empty but the pointer is on the sentinel
      // So there is to possibilities : either we end to read thebuffer is safe mode and
      // we need to re-initialise the safe buffer or we need just to directly pass to the first element of the buffer
      if (safe_pos == &buffer) {
        if (list_was_read == 0) {
          tmp = list_first_entry (&buffer, Case, list);
          safe_pos = &tmp->list;
          list_was_read = 1;
        } else {
          list_was_read = 0;
          list_for_each_safe(pos, q, &buffer){ //To reinitialise read_safe pointers
           tmp = list_entry(pos, Case, list);
           tmp->r_s = tmp->r;
          };
          return 0;
        }
      }

      c = list_entry(safe_pos, Case, list);
      sizeToRead = MIN(c->size, size);
      if (copy_to_user(buf, c->data + c->r_s, sizeToRead) == 0) {
        c->r_s += sizeToRead;
        if (c->size - 1 < c->r_s) { //all data from current case is read -> next pass to next Case
          //2 scenario : it was last element in the list and... not
          if (list_is_last(safe_pos, &buffer)) {
            safe_pos = &buffer;
            list_was_read = 1;
          } else { // It is not last
            tmp = list_entry(safe_pos, Case, list);
            c = list_next_entry(tmp, list);
            safe_pos = &c->list;
          }
        }
      } else {
        return -EFAULT;
      }

      printk(KERN_ALERT "[%d elements read from safe buffer]\n", (int) sizeToRead);
      printk(KERN_ALERT "[END READ SAFE BUFFER]\n");
    return sizeToRead;
}

static ssize_t TP6_read(struct file *f, char * buf, size_t size, loff_t *offset) {
    Case * c, *tmp;
    size_t sizeToRead;
    sizeToRead = 0;
    //List empty -> 0 byte read
      if (list_empty(&buffer))
        return 0;

      c = list_first_entry (&buffer, Case, list);
      sizeToRead = MIN(c->size, size);
      printk(KERN_ALERT "[READ BUFFER]\n");
      if (copy_to_user(buf, c->data + c->r, sizeToRead) == 0) {
        c->size -= sizeToRead;
        c->r += sizeToRead;

        // Is safe read pointer is smaller that normal read pointer -
        // we have to equelize it
        if (c->r_s < c->r) {
          c->r_s = c->r;
        }

        if (c->size <= 0) { //all data from current case is read -> delete case
          //If this case was also the save_pos - we have to push it to the next one
          if (&c->list == safe_pos) {
            tmp = list_next_entry(c, list);
            safe_pos = &tmp->list;
          }
          list_del(&c->list);
          delete_case(c);
          // If list is empty - reinitialise safe_pos
          if (list_empty(&buffer)) {
            safe_pos = &buffer;
            list_was_read = 0;
          }
          //printk(KERN_ALERT "[Case deleted]\n");
        }
      } else {
        return -EFAULT;
      }
      printk(KERN_ALERT "[%d elements read]\n", (int) sizeToRead);
      printk(KERN_ALERT "[END READ BUFFER]\n");
    return sizeToRead;
}


static ssize_t TP6_write(struct file *f, const char * buf, size_t size, loff_t *offset) {
  Case *n;
  //Standart write procedure
  printk(KERN_ALERT "[WRITE IN BUFFER]\n");
  if ((n = create_case(buf, size)) != NULL) {
    INIT_LIST_HEAD(&n->list);
    list_add_tail(&n->list, &buffer); //append buffer's tail with new element
    print_buffer(NULL);
    printk(KERN_ALERT "[END WRITE IN BUFFER]\n");
    return size;
  } else
  return 0;
}

static int TP6_open(struct inode *inode, struct file *f) {
    if (iminor(inode) == READ_DEVICE) {
      fops.write = NULL;
      fops.read = TP6_read;
    } else if (iminor(inode) == READ_DEVICE_SAFE) {
      fops.write = NULL;
      fops.read = TP6_read_safe;
    } else {
      fops.read = NULL;
      fops.write = TP6_write;
    }
    printk(KERN_ALERT "[OPEN %s FILE]", (iminor(inode) == 0)?"R":(iminor(inode) == 2)?"SAFE R":"W");
    return 0;
}

static int TP6_release(struct inode * inode, struct file *f) {
    printk(KERN_ALERT "[CLOSE %s FILE]\n", (iminor(inode) == 0)?"R":(iminor(inode) == 2)?"SAFE R":"W");
    return 0;
}

static int bu_init_module(void){
    printk(KERN_ALERT "[INIT MODULE TP6]\n");
    /*allocation dynamique pour les paires (majors)*/
    if(alloc_chrdev_region(&dev,0,3,"TP6") == -1) {
      printk(KERN_ALERT "[ERROR] : alloc_chrdev_region\n");
      return -EINVAL;
    }
    /* recuperation et affichage */
    printk(KERN_ALERT "[INFO] given major %d and minor %d\n",MAJOR(dev),MINOR(dev));
    /* allocation des structures pour les operations */
    user_cdev = cdev_alloc();
    user_cdev->ops = &fops;
    user_cdev->owner = THIS_MODULE;
    /* lien entre operations et periph */
    cdev_add(user_cdev,dev,3);

    // Init list
    INIT_LIST_HEAD(&buffer); //init new buffer on INIT, not open
    safe_pos = &buffer;
    pos = NULL;
    q = NULL;

    return 0;
}


static void bu_cleanup_module(void){
    // We must free and clear buffer on cleanup
    Case * tmp;
    struct list_head * q;
    printk(KERN_ALERT "[CLEANUP MODULE TP6]\n");
    list_for_each_safe(pos, q, &buffer){
     tmp = list_entry(pos, Case, list);
     list_del(pos);
     delete_case(tmp);
    }

    /* liberation */
    unregister_chrdev_region(dev,3);
    cdev_del(user_cdev);
}


/* SET UP */

module_init(bu_init_module);
module_exit(bu_cleanup_module);

MODULE_LICENSE(LICENCE);
MODULE_AUTHOR(AUTHOR);
MODULE_DESCRIPTION(DESCRIPTION);
MODULE_SUPPORTED_DEVICE(DEVICE);
