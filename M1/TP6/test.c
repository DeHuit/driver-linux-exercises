#include <linux/list.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*Structure and operators*/
typedef struct s_case{
    char * data;   //data in case
    size_t size;   //current data size
    size_t r;      //current read position
    size_t r_s;      //current read position
    struct list_head list;
} Case;

Case * create_case(const char * data, size_t size) {
  //Step 1 : allocate and set up Case structure
  Case * n;
  n = (Case *) malloc(sizeof(Case));
  n->r = 0;
  n->r_s = 0;
  n->size = size;
  n->data = (char *) malloc(sizeof(char) * size);
  if (strcpy(n->data, data, sizeof(char)*size) == 0) {
    printf("New node created\n");
    return n;
  } else {
    free(n->data);
    free(n);
    printk("New node creation failed\n");
    return NULL;
  }
}

ssize_t delete_case(Case *c) {
  free(c->data);
  free(c);
  return 0;
}

struct list_head buffer;

char * str = "string";
#define SIZE = 7

int main(int argc, char const *argv[]) {
  Case * n = create_case(str, SIZE)
  INIT_LIST_HEAD(&n->list);
  list_add_tail(&n->list, &buffer);
  return 0;
}
