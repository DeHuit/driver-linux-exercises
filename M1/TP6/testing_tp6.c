#include <fcntl.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
# include <string.h>
#include <unistd.h>

#define DATA "abcd"
#define DATA_1 "1234"
#define DATA_2 "???"
#define DATA_3 "yes"
#define DATA_SIZE 4
#define DATA_1_SIZE 4
#define DATA_2_SIZE 3
#define DATA_3_SIZE 3

#define READ_SIZE 2
#define READ_SIZE_1

int main (int argc, char *argv[]) {
    // Declare the variable for the data to be read from file
    char data[50] = DATA;
    char data1[50] = DATA_1;
    char data2[50] = DATA_2;
    char data3[50] = DATA_3;
    char dataToBeRead[50];
    //char module_dir[50];

    int filePointer;

    // if (argc != 2) {
    //   fprintf(stderr, "%d Usage %s /dev/[module_name]\n", argc, argv[0]);
    //   return -1;
    // }

    filePointer = open("/dev/w" , O_RDWR) ;

    printf("The file is now opened.\n") ;
    printf("Writing \"%s\".\n", DATA ) ;
    data[DATA_SIZE] = '\0';
    write(filePointer, data, DATA_SIZE);

    // printf("Writing \"%s\".\n", DATA_1 ) ;
    // data1[DATA_1_SIZE] = '\0';
    // write(filePointer, data1, DATA_1_SIZE);
    //
    // printf("Writing \"%s\".\n", DATA_2 ) ;
    // data1[DATA_2_SIZE] = '\0';
    // write(filePointer, data2, DATA_2_SIZE);

    close(filePointer);

    filePointer = open("/dev/r" , O_RDWR) ;
    int sizeRead = read(filePointer, dataToBeRead, 2);
    dataToBeRead[sizeRead] = '\0';
    if (sizeRead > 0) {
      printf("Data read result from normal read : [%d byte] : %s\n", sizeRead, dataToBeRead);
    } else {
      printf("No data found by normal read\n");
    }
    close(filePointer) ;

    // filePointer = open("/dev/r" , O_RDWR) ;
    // sizeRead = read(filePointer, dataToBeRead, DATA_SIZE);
    // dataToBeRead[sizeRead] = '\0';
    // if (sizeRead > 0) {
    //   printf("Data read result : [%d byte] : %s\n", sizeRead, dataToBeRead);
    // } else {
    //   printf("No data found\n");
    // }
    // close(filePointer);

    filePointer = open("/dev/r_s" , O_RDWR) ;
    sizeRead = read(filePointer, dataToBeRead, 2);
    dataToBeRead[sizeRead] = '\0';
    if (sizeRead > 0) {
      printf("Data read result by safe read : [%d byte] : %s\n", sizeRead, dataToBeRead);
    } else {
      printf("No data found\n");
    }
    close(filePointer) ;

    filePointer = open("/dev/r_s" , O_RDWR) ;
    sizeRead = read(filePointer, dataToBeRead, 2);
    dataToBeRead[sizeRead] = '\0';
    if (sizeRead > 0) {
      printf("Data read result : [%d byte] : %s\n", sizeRead, dataToBeRead);
    } else {
      printf("No data found\n");
    }
    close(filePointer) ;

    filePointer = open("/dev/r_s" , O_RDWR) ;
    sizeRead = read(filePointer, dataToBeRead, 2);
    dataToBeRead[sizeRead] = '\0';
    if (sizeRead > 0) {
      printf("Data read result : [%d byte] : %s\n", sizeRead, dataToBeRead);
    } else {
      printf("No data found\n");
    }
    close(filePointer) ;

    filePointer = open("/dev/r_s" , O_RDWR) ;
    sizeRead = read(filePointer, dataToBeRead, 2);
    dataToBeRead[sizeRead] = '\0';
    if (sizeRead > 0) {
      printf("Data read result : [%d byte] : %s\n", sizeRead, dataToBeRead);
    } else {
      printf("No data found\n");
    }
    close(filePointer) ;

    printf("The file is now closed.\n") ;

    return 0;
}
