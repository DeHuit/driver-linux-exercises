#include <linux/init.h>
#include <linux/module.h>
#include <linux/cdev.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/list.h>
#include <linux/slab.h>

#define LICENCE "GPL V2"
#define AUTHOR "Nikita Kruglov nikita.kruglov@univ-tlse3.fr"
#define DESCRIPTION "TP4"
#define DEVICE "Mi Laptop pro"

#define MIN(a,b) (((a)<(b))?(a):(b))

// Declaration de fonctions (prefixe par bu)
static ssize_t tp4_read(struct file *, char *, size_t, loff_t *);
static ssize_t tp4_write(struct file *, const char *, size_t, loff_t *);
static int tp4_open(struct inode *, struct file *);
static int tp4_release(struct inode *, struct file *);

//Structure de file_operations
static struct file_operations fops = {
    .read = tp4_read,
    .write = tp4_writesc,
    .open = tp4_open,
    .release = tp4_release,
};

dev_t dev;
struct cdev * user_cdev;
char first_write_flag, need_to_init_buffer;

struct list_head buffer;
struct list_head *pos, *q;

/*Structure and operators*/
typedef struct s_case{
    char * data;   //data in case
    size_t size;   //current data size
    size_t r;   //current read position
    struct list_head list;
} Case;

/* Operators */

Case * create_case(const char * data, size_t size) {
  //Step 1 : allocate and set up Case structure
  Case * n;
  n = (Case *) kmalloc(sizeof(Case), GFP_KERNEL);
  n->r = 0;
  n->size = size;
  n->data = (char *) kmalloc(sizeof(char) * size, GFP_KERNEL);
  if (copy_from_user(n->data, data, sizeof(char)*size) == 0) {
    printk(KERN_ALERT "New node created");
    return n;
  } else {
    kfree(n->data);
    kfree(n);
    printk(KERN_ALERT "New node creation failed");
    return NULL;
  }
}

ssize_t delete_case(Case *c) {
  kfree(c->data);
  kfree(c);
  return 0;
}

void print_buffer(const char * s) {
    ssize_t i = 0;
    Case * unElement;
    char * buffer_to_str;
    struct list_head *ptr;
    printk(KERN_ALERT "[PRINTING BUFFER]");
    list_for_each(ptr, &buffer) {
      unElement = list_entry(ptr, Case, list);
      buffer_to_str = kmalloc(unElement->size+1, GFP_KERNEL);
      for(i = 0; i < unElement->size; i++)
        buffer_to_str[i] = unElement->data[i];
      buffer_to_str[unElement->size] = '\0';
      printk(KERN_ALERT "%s\n", buffer_to_str);
      kfree(buffer_to_str);
    };
    printk(KERN_ALERT "[END PRINTING BUFFER]\n");
}

/* IMPLEMENTATION OF FILE OPERATIONS */

static ssize_t tp4_read(struct file *f, char * buf, size_t size, loff_t *offset) {
    Case * c;
    size_t sizeToRead;
    char * tst;
    //List empty -> 0 byte read
    if (list_empty(&buffer))
      return 0;

    c = list_first_entry (&buffer, Case, list);
    print_buffer("(BEFORE READ)");
    sizeToRead = MIN(c->size, size);
    tst = kmalloc(sizeToRead+1*sizeof(char), GFP_KERNEL);//info to print beffuer's content
    printk(KERN_ALERT "\n[READ BUFFER]\n");
    if (copy_to_user(buf, c->data + c->r, sizeToRead) == 0) {
      c->size -= sizeToRead;
      c->r += sizeToRead;
      if (c->size <= 0) { //all data from current case is read -> delete case
        list_del(&c->list);
        delete_case(c);
        printk(KERN_ALERT "[Case deleted]\n");
      }
    } else
      return -EFAULT;

    printk(KERN_ALERT "[%d elements read]\n", (int) sizeToRead);
    printk(KERN_ALERT "[END READ BUFFER]\n");
    return sizeToRead;
}


static ssize_t tp4_write(struct file *f, const char * buf, size_t size, loff_t *offset) {
  // Case * tmp, *n;
  // struct list_head * q;

  //Flag that indicates that after open we should clear buffer
  if (first_write_flag == 1) {
    first_write_flag = 0;
    printk(KERN_ALERT "[OPEN DETECTED : BUFFER CLEARING]\n");
    //Don't need to clear buffer if is already empty
    //(that can also mey meaning that is was just initialised)
    // if (!list_empty(&buffer)) {
    //   list_for_each_safe(pos, q, &buffer){
    //    tmp = list_entry(pos, Case, list);
    //    list_del(pos);
    //    delete_case(tmp);
    //  };
    // }
  }

  //Standart write procedure
  printk(KERN_ALERT "\n[WRITE IN BUFFER]\n");
  if ((n = create_case(buf, size)) != NULL) {
    INIT_LIST_HEAD(&n->list);
    list_add_tail(&n->list, &buffer); //append buffer's tail with new element
    print_buffer(NULL);
    printk(KERN_ALERT "[END WRITE IN BUFFER]\n");
    return size;
  } else
  return 0;
}

static int tp4_open(struct inode *inode, struct file *f) {
    printk(KERN_ALERT "[OPEN FILE]");
    first_write_flag = 1; //Open() triggers buffer clean up on next write
    return 0;
}

static int tp4_release(struct inode * inode, struct file *f) {
  printk(KERN_ALERT "[CLOSE FILE]\n");
  return 0;
}

static int bu_init_module(void){
    printk(KERN_ALERT "[INIT MODULE TP4]\n");
    /*allocation dynamique pour les paires (majors)*/
    if(alloc_chrdev_region(&dev,0,1,"buffer") == -1) {
      printk(KERN_ALERT "[ERROR] : alloc_chrdev_region\n");
      return -EINVAL;
    }
    /* recuperation et affichage */
    printk(KERN_ALERT "[INFO] given major %d and minor %d\n",MAJOR(dev),MINOR(dev));
    /* allocation des structures pour les operations */
    user_cdev = cdev_alloc();
    user_cdev->ops = &fops;
    user_cdev->owner = THIS_MODULE;
    /* lien entre operations et periph */
    cdev_add(user_cdev,dev,1);

    // Init list
    INIT_LIST_HEAD(&buffer); //init new buffer on INIT, not open
    pos = NULL;
    q = NULL;

    return 0;
}


static void bu_cleanup_module(void){
    // We must free and clear buffer on cleanup
    Case * tmp;
    struct list_head * q;
    printk(KERN_ALERT "[CLEANUP MODULE TP4]\n");
    list_for_each_safe(pos, q, &buffer){
     tmp = list_entry(pos, Case, list);
     list_del(pos);
     delete_case(tmp);
    }

    /* liberation */
    unregister_chrdev_region(dev,1);
    cdev_del(user_cdev);
}


/* SET UP */

module_init(bu_init_module);
module_exit(bu_cleanup_module);

MODULE_LICENSE(LICENCE);
MODULE_AUTHOR(AUTHOR);
MODULE_DESCRIPTION(DESCRIPTION);
MODULE_SUPPORTED_DEVICE(DEVICE)
