#include <linux/init.h>
#include <linux/module.h>
#include <linux/cdev.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/mutex.h>

#define LICENCE "GPL V2"
#define AUTHOR "Nikita Kruglov nikita.kruglov@univ-tlse3.fr"
#define DESCRIPTION "TP8"
#define DEVICE "Mi Laptop pro"

#define READ_DEVICE 0
#define READ_DEVICE_SAFE 1
#define WRITE_DEVICE 2

#define MIN(a,b) (((a)<(b))?(a):(b))

// Declaration de fonctions (prefixe par bu)
static ssize_t TP8_read(struct file *, char *, size_t, loff_t *);
static ssize_t TP8_write(struct file *, const char *, size_t, loff_t *);
static int TP8_open(struct inode *, struct file *);
static int TP8_release(struct inode *, struct file *);

//Structure de file_operations
static struct file_operations fops = {
    .read = TP8_read,
    .write = TP8_write,
    .open = TP8_open,
    .release = TP8_release,
};

dev_t dev;
struct cdev * user_cdev;
char need_to_init_buffer;

struct list_head envs;

typedef struct s_environment {
  int pid;
  struct list_head buffer;
  struct list_head * safe_pos, *q, *pos;
  char list_was_read; //flag
  struct list_head list;
} Environment;
Environment * current_environment;

/*Structure and operators*/
typedef struct s_case{
    char * data;   //data in case
    size_t size;   //current data size
    size_t r;      //current read position
    size_t r_s;      //current safe read position
    struct list_head list;
} Case;

/* Operators */

Environment * new_env(int pid) {
  Environment * e;
  e = (Environment *) kmalloc(sizeof(Environment), GFP_KERNEL);
  e->pid = pid;
  INIT_LIST_HEAD(&e->buffer);
  e->safe_pos = &e->buffer;
  e->q = NULL;
  e->pos = NULL;
  e->list_was_read = 0;
  return e;
}

void delete_env(Environment * e) {
  kfree(e);
}

Case * create_case(const char * data, size_t size) {
  //Step 1 : allocate and set up Case structure
  Case * n;
  n = (Case *) kmalloc(sizeof(Case), GFP_KERNEL);
  n->r = 0;
  n->r_s = 0;
  n->size = size;
  n->data = (char *) kmalloc(sizeof(char) * size, GFP_KERNEL);
  if (copy_from_user(n->data, data, sizeof(char)*size) == 0) {
    printk(KERN_ALERT "New node created");
    return n;
  } else {
    kfree(n->data);
    kfree(n);
    printk(KERN_ALERT "New node creation failed");
    return NULL;
  }
}

ssize_t delete_case(Case *c) {
  if (c->size > 0)
    kfree(c->data);
  kfree(c);
  return 0;
}

void print_buffer(const char * s) {
    ssize_t i = 0;
    Case * unElement;
    char * buffer_to_str;
    struct list_head *ptr;
    printk(KERN_ALERT "[PRINTING BUFFER]\n");
    list_for_each(ptr, &current_environment->buffer) {
      unElement = list_entry(ptr, Case, list);
      buffer_to_str = kmalloc(unElement->size+1, GFP_KERNEL);
      for(i = 0; i < unElement->size; i++)
        buffer_to_str[i] = unElement->data[i];
      buffer_to_str[unElement->size] = '\0';
      printk(KERN_ALERT "%s\n", buffer_to_str);
      kfree(buffer_to_str);
    };
    printk(KERN_ALERT "[END PRINTING BUFFER]\n");
}

/* IMPLEMENTATION OF FILE OPERATIONS */
//Flag = 0 -> list wasn't read is the safe mannier yet
//Flag = 1 -> list was read and we need to re-init our safe read device at the end
//char list_was_read = 0;

static ssize_t TP8_read_safe(struct file *f, char * buf, size_t size, loff_t *offset) {
    Case * c, *tmp;
    size_t sizeToRead;
    struct list_head * q, *pos;

    sizeToRead = 0;
    //List empty -> 0 byte read
    //printk(KERN_ALERT "[READ BUFFER] : %p empty? %d\n", &current_environment->buffer, list_empty(&current_environment->buffer));
      if (list_empty(&current_environment->buffer)) {
        return 0;
      }
      // Cae when list is not empty but the pointer is on the sentinel
      // So there is to possibilities : either we end to read thebuffer is safe mode and
      // we need to re-initialise the safe buffer or we need just to directly pass to the first element of the buffer
      if (current_environment->safe_pos == &current_environment->buffer) {
        if (current_environment->list_was_read == 0) {
          tmp = list_first_entry (&current_environment->buffer, Case, list);
          current_environment->safe_pos = &tmp->list;
          current_environment->list_was_read = 1;
        } else {
          current_environment->list_was_read = 0;
          list_for_each_safe(pos, q, &current_environment->buffer){ //To reinitialise read_safe pointers
           tmp = list_entry(pos, Case, list);
           tmp->r_s = tmp->r;
          };
          return 0;
        }
      }

      c = list_entry(current_environment->safe_pos, Case, list);
      sizeToRead = MIN(c->size, size);
      if (copy_to_user(buf, c->data + c->r_s, sizeToRead) == 0) {
        c->r_s += sizeToRead;
        if (c->size - 1 < c->r_s) { //all data from current case is read -> next pass to next Case
          //2 scenario : it was last element in the list and... not
          if (list_is_last(current_environment->safe_pos, &current_environment->buffer)) {
            current_environment->safe_pos = &current_environment->buffer;
            current_environment->list_was_read = 1;
          } else { // It is not last
            tmp = list_entry(current_environment->safe_pos, Case, list);
            c = list_next_entry(tmp, list);
            current_environment->safe_pos = &c->list;
          }
        }
      } else {
        return -EFAULT;
      }

      printk(KERN_ALERT "[%d elements read from safe buffer]\n", (int) sizeToRead);
      printk(KERN_ALERT "[END READ SAFE BUFFER]\n");
    return sizeToRead;
}

static ssize_t TP8_read(struct file *f, char * buf, size_t size, loff_t *offset) {
    Case * c, *tmp;
    size_t sizeToRead;
    int err;
    sizeToRead = 0;
    //List empty -> 0 byte read
    //printk(KERN_ALERT "[READ BUFFER] : %p empty? %d\n", &current_environment->buffer, list_empty(&current_environment->buffer));
      if (list_empty(&current_environment->buffer))
        return 0;

      c = list_first_entry (&current_environment->buffer, Case, list);
      sizeToRead = MIN(c->size, size);

      if ((err = copy_to_user(buf, c->data + c->r, sizeToRead)) == 0) {
        c->size -= sizeToRead;
        c->r += sizeToRead;
        // Is safe read pointer is smaller that normal read pointer -
        // we have to equelize it
        if (c->r_s < c->r) {
          c->r_s = c->r;
        }

        if (c->size <= 0) { //all data from current case is read -> delete case
          //If this case was also the save_pos - we have to push it to the next one
          if (&c->list == current_environment->safe_pos) {
            tmp = list_next_entry(c, list);
            current_environment->safe_pos = &tmp->list;
          }
          list_del(&c->list);
          delete_case(c);
          // If list is empty - reinitialise safe_pos
          if (list_empty(&current_environment->buffer)) {
            current_environment->safe_pos = &current_environment->buffer;
            current_environment->list_was_read = 0;
          }
        }
      } else {
        printk(KERN_ALERT "[EROOR READING] : error code = %d \n", err);
        return -EFAULT;
      }
      printk(KERN_ALERT "[%d elements read]\n", (int) sizeToRead);
      printk(KERN_ALERT "[END READ BUFFER]\n");
    return sizeToRead;
}


static ssize_t TP8_write(struct file *f, const char * buf, size_t size, loff_t *offset) {
  Case *n;
  //Standart write procedure
  printk(KERN_ALERT "[WRITE IN BUFFER]\n");
  if ((n = create_case(buf, size)) != NULL) {
    INIT_LIST_HEAD(&n->list);
    list_add_tail(&n->list, &current_environment->buffer); //append buffer's tail with new element
    print_buffer(NULL);
    printk(KERN_ALERT "[END WRITE IN BUFFER]\n");
    return size;
  } else
  return 0;
}


static DEFINE_MUTEX(mutex);

static int TP8_open(struct inode *inode, struct file *f) {
    Environment * e = NULL;
    struct list_head * pos, *q;
    char found = 0;

    // Exlusion
    printk(KERN_ALERT "[PROCESS %i ARRIVED TO MUTEX]\n", current->pid);
    mutex_lock(&mutex);
    printk(KERN_ALERT "[PROCESS %i NOW AFTER MUTEX]\n", current->pid);

    if (iminor(inode) == READ_DEVICE) {
      fops.write = NULL;
      fops.read = TP8_read;
    } else if (iminor(inode) == READ_DEVICE_SAFE) {
      fops.write = NULL;
      fops.read = TP8_read_safe;
    } else {
      fops.read = NULL;
      fops.write = TP8_write;
    }
    printk(KERN_ALERT "\n[OPEN %s FILE by %i]", (iminor(inode) == READ_DEVICE)?"R":(iminor(inode) == READ_DEVICE_SAFE)?"SAFE R":"W", current->pid);

    list_for_each_safe(pos, q, &envs){
      e = list_entry(pos, Environment, list);
      if (e->pid == current->pid) {
        found = 1;
        current_environment = e;
      }
    };

    // New process! yay!
    if (found == 0) {
      e = new_env(current->pid);
      INIT_LIST_HEAD(&e->list);
      list_add_tail(&e->list, &envs);
      current_environment = e;
      printk(KERN_ALERT "[NEW PROCESS %i ARRIVED]\n", current->pid);
    }

    return 0;
}

static int TP8_release(struct inode * inode, struct file *f) {
    printk(KERN_ALERT "[CLOSE %s FILE]\n", (iminor(inode) == READ_DEVICE)?"R":(iminor(inode) == READ_DEVICE_SAFE)?"SAFE R":"W");
    mutex_unlock(&mutex);
    return 0;
}

static int bu_init_module(void){
    printk(KERN_ALERT "[INIT MODULE TP8]\n");
    /*allocation dynamique pour les paires (majors)*/
    if(alloc_chrdev_region(&dev,0,3,"TP8") == -1) {
      printk(KERN_ALERT "[ERROR] : alloc_chrdev_region\n");
      return -EINVAL;
    }
    /* recuperation et affichage */
    printk(KERN_ALERT "[INFO] given major %d and minor %d to read with distruction\n",MAJOR(dev),MINOR(dev));
    printk(KERN_ALERT "[INFO] given major %d and minor %d to read without distruction\n",MAJOR(dev),MINOR(dev)+1);
    printk(KERN_ALERT "[INFO] given major %d and minor %d to write\n",MAJOR(dev),MINOR(dev)+2);
    /* allocation des structures pour les operations */
    user_cdev = cdev_alloc();
    user_cdev->ops = &fops;
    user_cdev->owner = THIS_MODULE;
    /* lien entre operations et periph */
    cdev_add(user_cdev,dev,3);

    // Init list
    INIT_LIST_HEAD(&envs); //init new buffer on INIT, not open

    return 0;
}


static void bu_cleanup_module(void){
    // We must free and clear buffer on cleanup
    Case * tmp_c;
    Environment * tmp_e;
    struct list_head * q_e;
    struct list_head * q_c;
    struct list_head * pos_e;
    struct list_head * pos_c;

    printk(KERN_ALERT "[CLEANUP MODULE TP8]\n");
    if (!list_empty(&envs)) {
      list_for_each_safe(pos_e, q_e, &envs){
       tmp_e = list_entry(pos_e, Environment, list);
       printk(KERN_ALERT "CLEANING UP ENV #%i\n", tmp_e->pid);
       if (!list_empty(&tmp_e->buffer)) {
         list_for_each_safe(pos_c, q_c, &(tmp_e->buffer)){
          tmp_c = list_entry(pos_c, Case, list);
          list_del(pos_c);
          delete_case(tmp_c);
         }
       }
       list_del(pos_e);
       delete_env(tmp_e);
      }
    }

    /* liberation */
    unregister_chrdev_region(dev,3);
    cdev_del(user_cdev);
}


/* SET UP */

module_init(bu_init_module);
module_exit(bu_cleanup_module);

MODULE_LICENSE(LICENCE);
MODULE_AUTHOR(AUTHOR);
MODULE_DESCRIPTION(DESCRIPTION);
MODULE_SUPPORTED_DEVICE(DEVICE);
