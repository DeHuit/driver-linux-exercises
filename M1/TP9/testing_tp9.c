#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
# include <string.h>
#include <unistd.h>
#include <linux/ioctl.h>

#define SAMPLE_IOC_MAGIC 'k'
#define SAMPLE_IOCRESET _IO(SAMPLE_IOC_MAGIC, 0)

#define DATA "abcd"
#define DATA_1 "1234"
#define DATA_2 "???"
#define DATA_3 "yes"
#define DATA_SIZE 4
#define DATA_1_SIZE 4
#define DATA_2_SIZE 3
#define DATA_3_SIZE 3

#define READ_SIZE 2
#define READ_SIZE_1

int filePointer;

void rd(int size) {
  char dataToBeRead[size+2];
  printf("Attempt to read %d byte from buffer\n", size);
  int sizeRead = read(filePointer, dataToBeRead, size);
  dataToBeRead[sizeRead] = '\0';
  if (sizeRead > 0) {
    printf("Data read result : [%d byte] : %s\n", sizeRead, dataToBeRead);
  } else {
    printf("No data found\n");
  }
}

void wr(char * str, int size) {
  char data[50];
  memcpy(data, str, size);
  data[size] = '\0';
  printf("Writing \"%s\".\n", str) ;
  write(filePointer, data, size);
  //free(data);
}

void do_reset(char * fname) {
  printf("Doing ioctl reset on %s\n", fname);
  ioctl(filePointer,SAMPLE_IOCRESET,0);
}

int main (int argc, char *argv[]) {
    // Declare the variable for the data to be read from file
    char data[50] = DATA;
    char data1[50] = DATA_1;
    char data2[50] = DATA_2;
    char data3[50] = DATA_3;

    //char module_dir[50];

    if (argc != 2) {
      fprintf(stderr, "%d Usage %s /dev/[module_name]\n", argc, argv[0]);
      return -1;
    }

    filePointer = open(argv[1] , O_RDWR) ;

    char * input;
    char choice = '1';
    while (choice != 'q' && choice != 'Q') {
      printf("[0] Do you wonna 'R'ead, 'W'rite, r'E'set or 'Q'uit?\n");
      choice = getchar();
      getchar();
      if (choice == 'q' || choice == 'Q')
        break;
      else if (choice == 'r' || choice == 'R') {
        printf("[1] Enter number of bytes to read (1-9):\n");
        choice = getchar();
        getchar();
        rd(choice - '0');
      } else if (choice == 'w' || choice == 'W') {
        printf("[1] Enter sequence to write :\n");
        int n = 0;
        while(n < 45) {
           if ((input[n++] = getchar()) == '\n')
              break;
        }
        input[n-1] = '\0';
        wr(input, strlen(input));
      } else if (choice == 'e' || choice == 'E') {
        do_reset(argv[1]);
      }
    }

    close(filePointer) ;
    printf("The file is now closed.\n") ;

    return 0;
}
