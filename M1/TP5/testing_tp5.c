#include <fcntl.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
# include <string.h>
#include <unistd.h>

#define DATA "abcd"
#define DATA_1 "1234"
#define DATA_2 "???"
#define DATA_3 "yes"
#define DATA_SIZE 4
#define DATA_1_SIZE 4
#define DATA_2_SIZE 3
#define DATA_3_SIZE 3

#define READ_SIZE 2
#define READ_SIZE_1

int main (int argc, char *argv[]) {
    // Declare the variable for the data to be read from file
    char data[50] = DATA;
    char data1[50] = DATA_1;
    char data2[50] = DATA_2;
    char data3[50] = DATA_3;
    char dataToBeRead[50];
    //char module_dir[50];

    int filePointerW, filePointerR;

    if (argc != 3) {
      fprintf(stderr, "%d Usage %s /dev/[module_ecriture] /dev/[module_lecture]\n", argc, argv[0]);
      return -1;
    }
    //printf("lalala\n");
    filePointerW = open(argv[1], O_WRONLY) ;

    //if (filePointer < 0) {
    //  fprintf(stderr, "Usage %s /dev/[module_name]\n", argv[0]);
    //  return -1;
    //}

    printf("The file is now opened.\n") ;
    printf("Writing \"%s\".\n", DATA ) ;
    data[DATA_SIZE] = '\0';
    write(filePointerW, data, DATA_SIZE);
    printf("Writing \"%s\".\n", DATA_1 ) ;
    data1[DATA_1_SIZE] = '\0';
    write(filePointerW, data1, DATA_1_SIZE);
    printf("Writing \"%s\".\n", DATA_2 ) ;
    data1[DATA_2_SIZE] = '\0';
    write(filePointerW, data2, DATA_2_SIZE);
    close(filePointerW) ;

    filePointerR = open(argv[2], O_RDONLY) ;
    for (int i = 0; i < 5; i++) {
      printf("Attempt to read %d byte from buffer\n", i);
      int sizeRead = read(filePointerR, dataToBeRead, i);
      dataToBeRead[sizeRead] = '\0';
      if (sizeRead > 0) {
        printf("Data read result : [%d byte] : %s\n", sizeRead, dataToBeRead);
      } else {
        printf("No data found\n");
      }
    }
    close(filePointerR) ;
    printf("The file is now closed.\n") ;


    printf("\n\nNow re-opening the buffer.\n") ;
    printf("Firstly, teting read() : \"??\" should be found.\n");
    filePointerR = open(argv[2], O_RDONLY) ;
    int sizeRead = read(filePointerR, dataToBeRead, READ_SIZE);
    dataToBeRead[sizeRead] = '\0';
    if (sizeRead > 0) {
      printf("Data read result : [%d byte] : %s\n", sizeRead, dataToBeRead);
    } else {
      printf("No data found\n");
    }
    close(filePointerR);

    printf("Now trying to write(\"yes\"). so buffer should contain \"?\" and \"yes\" now\n");
    data3[DATA_3_SIZE] = '\0';
    filePointerW = open(argv[1], O_WRONLY) ;
    write(filePointerW, data3, DATA_3_SIZE);
    close(filePointerW);

    filePointerR = open(argv[2], O_RDONLY) ;
    sizeRead = read(filePointerR, dataToBeRead, DATA_3_SIZE);
    dataToBeRead[sizeRead] = '\0';
    if (sizeRead > 0) {
      printf("Data read result : [%d byte] : %s\n", sizeRead, dataToBeRead);
    } else {
      printf("No data found\n");
    }
    sizeRead = read(filePointerR, dataToBeRead, DATA_3_SIZE);
    dataToBeRead[sizeRead] = '\0';
    if (sizeRead > 0) {
      printf("Data read result : [%d byte] : %s\n", sizeRead, dataToBeRead);
    } else {
      printf("No data found\n");
    }

    close(filePointerR) ;

    return 0;
}
