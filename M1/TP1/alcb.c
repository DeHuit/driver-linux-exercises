/*
Nom : 
Prenom :
Compte examen :
*/

/* Includes */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/mutex.h>
#include <linux/device.h>


// Char driver functions 
static ssize_t sample_read(struct file *f, char *buf, size_t size, loff_t *offset);
static ssize_t sample_write(struct file *f, const char *buf, size_t size, loff_t *offset);
static int sample_open(struct inode *in, struct file *f);
static int sample_release(struct inode *in, struct file *f);


// standard file_ops for char driver 
static struct file_operations fops = 
{
  .read = sample_read,
  .write = sample_write,
  .open = sample_open,
  .release = sample_release,
};


// The dev_t for our device 
dev_t dev;

// The cdev for our device 
struct cdev *my_cdev;

static ssize_t sample_read(struct file *f, char *buf, size_t size, loff_t *offset) 
{

}

static ssize_t sample_write(struct file *f, const char *buf, size_t size, loff_t *offset) 
{

}

static int sample_open(struct inode *in, struct file *f) 
{

}

static int sample_release(struct inode *in, struct file *f) 
{

}

int sample_init(void) 
{
	// Dynamic allocation for (major,minor) 
	if (alloc_chrdev_region(&dev,0,2,"sample") == -1)
	{
		printk(KERN_ALERT ">>> ERROR alloc_chrdev_region\n");
		return -EINVAL;
	}
	// Print out the values 
	printk(KERN_ALERT "Init allocated (major, minor)=(%d,%d)\n",MAJOR(dev),MINOR(dev));

	// Structures allocation 
	my_cdev = cdev_alloc();
	my_cdev->ops = &fops;
	my_cdev->owner = THIS_MODULE;
	// linking operations to device 
	cdev_add(my_cdev,dev,2); 

	return(0);
}


static void sample_cleanup(void) 
{
	// Unregister 
	unregister_chrdev_region(dev,2);
	// Delete cdev 
	cdev_del(my_cdev);
}

module_exit(sample_cleanup);
module_init(sample_init);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("ModuleAuthor");
MODULE_DESCRIPTION("SampleDriver");
MODULE_SUPPORTED_DEVICE("MyDevice");

