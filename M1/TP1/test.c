#include <linux/module.h>	/* Needed by all modules */
#include <linux/kernel.h>	/* Needed for KERN_INFO */
#include <linux/module.h>	/* Needed by all modules */
#include <linux/kernel.h>	/* Needed for KERN_INFO */

static int init_module_hello(void)
{
	printk(KERN_INFO "Hello world\n");
	return 0;
}

static void cleanup_module_hello(void) {
  printk(KERN_INFO "Goodbye cruel world\n");
}

module_init(init_module_hello);
module_exit(cleanup_module_hello);

MODULE_LICENSE("GPL V2");
MODULE_AUTHOR("Nikita Kruglov");
MODULE_DESCRIPTION("Hello world linux kernel module");
MODULE_SUPPORTED_DEVICE("Xiaomi Laptop");
